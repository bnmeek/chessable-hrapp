@extends('layouts.app')

@section('content')
    <div class="employees">
        <h2 class="mb-3 pl-2">Employees</h2>
        <div class="row pl-5">
            <h5>Create Employee Record:</h5>
        </div>
        <div class="row pl-5 mb-5">
            <form method="POST" action="{{ route('createEmployee') }}">
                @csrf
                <div class="form-group">
                    <label for="first-name">First Name</label>
                    <input id="first-name" type="text" class="form-control" value="" name="first_name" placeholder="First Name">
                </div>
                <div class="form-group">
                    <label for="last-name">Last Name</label>
                    <input id="last-name" type="text" class="form-control" value="" name="last_name" placeholder="Last Name">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" value="" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="salary">Salary</label>
                    <input id="salary" type="number" class="form-control" value="" name="salary" placeholder="Salary">
                </div>
                <div class="form-group">
                    <label for="department-id">Department ID</label>
                    <input id="department-id" type="text" class="form-control" value="" name="dept_id" placeholder="Department ID">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>

            </form>
        </div>
        <div class="row pl-5 mb-2">
            <h5>Delete Employee Record:</h5>
        </div>
        <div class="row mt-5 ml-2">
            @foreach ($data as $emp)
                <div class="col-4">
                    <div class="mb-2 mr-2 card">
                        <div class="card-body">
                            <h5 class="card-title">{{$emp->first_name}} {{$emp->last_name}}</h5>
                            <p class="card-text"><strong>ID: </strong><span>{{$emp->id}}</span></p>
                            <p class="card-text"><strong>Email: </strong><span>{{$emp->email}}</span></p>
                            <p class="card-text"><strong>Salary: </strong><span>{{$emp->salary}}</span></p>
                            <p class="card-text"><strong>Department: </strong><span>{{$emp->dept_id}}</span></p>
                            <form method="POST" action="{{ route('deleteEmployee') }}">
                                <div class="form-group">
                                    @csrf
                                    @method('DELETE')
                                    <input id="id" type="hidden" class="form-control" value="{{$emp->id}}" name="id">
                                </div>
                                <button type="submit" class="btn btn-primary">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
@endsection
