@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12 pl-5">
            <h5>Hello!</h5>
            <p>Please use the menu above to view and manage the Employee and Department data and see the requested reports.</p>
            <p>This app was build in Laravel sail but without using the Eloquent ORM.  Database calls are either done using the DB facade
            or using DB::raw statements.  There is no Javascript involved in the functionality.</p>
            <p>The functionality to manage employees - including extra the ability to run UPDATE's is duplicated via an api available at /api/departments and /api/employees.
            The API requires a Bearer token to access, which is generated upon registering. (Although you will need access to the database to retrieve the token.)</p>

            <p>Please note that no extra functionality is exposed when logged in.</p>

            <p>Codebase available here: <a href="https://bitbucket.org/bnmeek/chessable-hrapp/src/master/" target="_blank">bitbucket</a></p>

            <p>Database created and seeded using the files in the 'database' dir</p>
            <p>Controllers are in 'app/Http/Controllers'</p>
            <p>Views are in 'resources/views'</p>

            <p>I spent around 4.5 hours building this app.</p>

            <p>Thanks! Ben</p>
     </div>
 </div>

@endsection
