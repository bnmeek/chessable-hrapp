@extends('layouts.app')

@section('content')
 <div class="row">
     <div class="col-1 pl-5 mb-5">
         <h2>Reports</h2>
     </div>
 </div>
 <h5 class="mt-2 ml-2">Top Salary pre department</h5>
 <div class="row mt-2 ml-2">
     @foreach($topSalary as $top)
         <div class="col-4">
             <div class="mb-2 mr-2 card">
                 <div class="card-body">
                     <h5 class="card-title">{{$top->name}}</h5>
                     <p class="card-text">Top Salary: {{number_format($top->top_salary)}}</p>
                 </div>
             </div>
         </div>
     @endforeach
 </div>

 <h5 class="mt-5 ml-2">Department earners over 50k</h5>
 <div class="row mt-2 ml-2">
     @foreach($over50k as $k => $v)
         <div class="col-4">
             <div class="mb-2 mr-2 card">
                 <div class="card-body">
                     <h5 class="card-title">{{$k}}</h5>
                        @foreach($v as $item)
                         <p class="card-text"><strong>Email: </strong>{{$item['email']}}</>
                         <p class="card-text"><strong>Salary: </strong>{{$item['salary']}}</p>
                     @endforeach
                 </div>
             </div>
         </div>
     @endforeach
 </div>

{{-- <div class="row pl-5 mb-5">--}}
{{--     <div class="col">--}}
{{--         <h5>Top salary by department</h5>--}}
{{--         <form method="GET" action="{{ route('salaryPerDepartment') }}">--}}
{{--             @csrf--}}
{{--             <button type="submit" class="btn btn-primary">Run</button>--}}
{{--         </form>--}}
{{--     </div>--}}
{{-- </div>--}}
{{-- <div class="row pl-5 mb-5">--}}
{{--     <div class="col">--}}
{{--         <h5>Departments with more than two +50k salaries</h5>--}}
{{--         <form method="GET" action="{{ route('departmentsWith50k') }}">--}}
{{--             @csrf--}}
{{--             <button type="submit" class="btn btn-primary">Run</button>--}}
{{--         </form>--}}
{{--     </div>--}}
{{-- </div>--}}
@endsection
