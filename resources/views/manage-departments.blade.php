@extends('layouts.app')

@section('content')
    <div class="departments">
        <h2 class="mb-3 pl-2">Departments</h2>
        <div class="row pl-5">
            <h5>Create Department Record:</h5>
        </div>
        <div class="row pl-5 mb-5">
            <form method="POST" action="{{ route('createDepartment') }}">
                @csrf
                <div class="form-group">
                    <label for="name">Department Name</label>
                    <input id="name" type="text" class="form-control" value="" name="name" placeholder="Department Name">
                </div>
                <div class="form-group">
                    <label for="department-id">Department ID</label>
                    <input id="department-id" type="text" class="form-control" value="" name="dept_id" placeholder="Department ID">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>

            </form>
        </div>
        <div class="row pl-5 mb-2">
            <h5>Delete Department Record:</h5>
        </div>
        <div class="row mt-5 ml-2">
            @foreach ($data as $dept)
                <div class="col-4">
                    <div class="mb-2 mr-2 card">
                        <div class="card-body">
                            <h5 class="card-title">{{$dept->name}}</h5>
                            <p class="card-text"><strong>Department: </strong><span>{{$dept->dept_id}}</span></p>
                            <form method="POST" action="{{ route('deleteDepartment') }}">
                                <div class="form-group">
                                    @csrf
                                    @method('DELETE')
                                    <input id="id" type="hidden" class="form-control" value="{{$dept->dept_id}}" name="dept_id">
                                </div>
                                <button type="submit" class="btn btn-primary">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
@endsection
