<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\DepartmentApiController;
use App\Http\Controllers\API\EmployeeApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/departments/{id}', [DepartmentApiController::class, 'show']);
Route::middleware('auth:api')->get('/departments', [DepartmentApiController::class, 'showAll']);
Route::middleware('auth:api')->post('/departments', [DepartmentApiController::class, 'create']);
Route::middleware('auth:api')->put('/departments/{id}', [DepartmentApiController::class, 'update']);
Route::middleware('auth:api')->delete('/departments/{id}', [DepartmentApiController::class, 'delete']);

Route::middleware('auth:api')->get('/employees/{id}', [EmployeeApiController::class, 'show']);
Route::middleware('auth:api')->get('/employees', [EmployeeApiController::class, 'showAll']);
Route::middleware('auth:api')->post('/employees', [EmployeeApiController::class, 'create']);
Route::middleware('auth:api')->put('/employees/{id}', [EmployeeApiController::class, 'update']);
Route::middleware('auth:api')->delete('/employees/{id}', [EmployeeApiController::class, 'delete']);
