<?php

use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/manage-departments', [DepartmentController::class, 'index'])->name('departmentsHome');
Route::get('/manage-employees', [EmployeeController::class, 'index'])->name('employeesHome');
Route::get('/reports', [ReportsController::class, 'index'])->name('reportsHome');

Route::get('/reports/1', [ReportsController::class, 'getSalaryPerDepartment'])->name('salaryPerDepartment');
Route::get('/reports/2', [ReportsController::class, 'getDepartmentsWith50k'])->name('departmentsWith50k');

Route::get('/departments/{id}', [DepartmentController::class, 'show']);
Route::get('/departments', [DepartmentController::class, 'showAll']);
Route::post('/departments', [DepartmentController::class, 'create'])->name('createDepartment');
Route::put('/departments', [DepartmentController::class, 'update'])->name('updateDepartment');;
Route::delete('/departments', [DepartmentController::class, 'delete'])->name('deleteDepartment');;

Route::get('/employees/{id}', [EmployeeController::class, 'show']);
Route::get('/employees', [EmployeeController::class, 'showAll']);
Route::post('/employees', [EmployeeController::class, 'create'])->name('createEmployee');;
Route::put('/employees', [EmployeeController::class, 'update'])->name('updateEmployee');;
Route::delete('/employees', [EmployeeController::class, 'delete'])->name('deleteEmployee');;
