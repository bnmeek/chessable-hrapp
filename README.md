#HR APP  
Rename .env.example to .env

Install dependencies:  
`composer install`

Start containers:  
`./vendor/bin/sail up -d`

Generate app key:
`./vendor/bin/sail artisan key:generate`

Create db tables:  
`./vendor/bin/sail artisan migrate`

Populate db:  
`./vendor/bin/sail artisan db:seed`

View:  
App available at http://localhost

Shut down app:  
`./vendor/bin/sail down -v`
