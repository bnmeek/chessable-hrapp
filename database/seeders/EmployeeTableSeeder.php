<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $dept_ids = DB::table('department')->pluck('dept_id');

        foreach (range(1, 20) as $index) {
            DB::table('employees')->insert([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->email,
                'salary' => $faker->numberBetween(18000, 150000),
                'dept_id' => $faker->randomElement($dept_ids),
            ]);
        }
    }
}
