<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(0, 5) as $index) {
            $dept_names = [
                'HR',
                'Development',
                'Management',
                'Accounts',
                'Operations',
                'Art'
            ];

            $dept_ids = [
                'HR1',
                'DEV',
                'MAN',
                'ACC',
                'OPS',
                'ART'
            ];
            DB::table('department')->insert([
                'name' => $dept_names[$index],
                'dept_id' => $dept_ids[$index],
            ]);
        }
    }
}
