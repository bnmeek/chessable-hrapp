<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    private $table;

    public function __construct()
    {
        $this->table = DB::table('employees');
    }

    public function index()
    {
        $data = $this->table->get()->all();
        return view('manage-employees', ['data' => $data]);
    }


    public function show($id)
    {
        $department = $this->table->where('t_id', $id)->first();
        return response()->json($department);
    }

    public function showAll()
    {
        return response()->json($this->table->get());
    }

    public function create(Request $request)
    {
        $this->table->insert([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'salary' => $request->get('salary'),
            'dept_id' => $request->get('dept_id')
        ]);
        return redirect()->route('employeesHome');
    }

    public function update($id, Request $request)
    {
        $department = $this->table->where('employee_id', $id)->update($request->all());
        return response()->json($department, 200);
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $this->table->where('id', $id)->delete();
        return redirect()->route('employeesHome');
    }
}
