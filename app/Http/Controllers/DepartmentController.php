<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{

    private $table;

    public function __construct()
    {
        $this->table = DB::table('department');
    }

    public function index()
    {
        $data = $this->table->get()->all();
        return view('manage-departments', ['data' => $data]);
    }

    public function show($id)
    {
        $department = $this->table->leftJoin('employees', 'department.dept_id', 'employees.dept_id')
            ->select('employees.*')->where('employees.dept_id', '=', $id)->get();

        return response()->json($department);
    }

    public function showAll()
    {
        return response()->json($this->table->get());
    }

    public function create(Request $request)
    {
        $this->table->insert(['name' => $request->get('name'), 'dept_id' => $request->get('dept_id')]);
        return redirect()->route('departmentsHome');
    }

    public function update($id, Request $request)
    {
        $department = $this->table->where('dept_id', $id)->update($request->all());
        return response()->json($department, 200);
    }

    public function delete(Request $request)
    {
        $id = $request->get('dept_id');
        $this->table->where('dept_id', $id)->delete();
        return redirect()->route('departmentsHome');
    }
}
