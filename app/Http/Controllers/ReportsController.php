<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ReportsController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        $topSalary = $this->getSalaryPerDepartment();
        $over50k = $this->getDepartmentsWith50k();

        return view('reports', compact(['topSalary', 'over50k']));
    }

    public function getSalaryPerDepartment()
    {
        $q = DB::select( DB::raw(
            "SELECT d.name,
            IFNULL((SELECT MAX(salary) FROM employees e WHERE d.dept_id = e.dept_id), 0) AS top_salary
            FROM department d
            LEFT JOIN employees e
            ON d.dept_id=e.dept_id
            GROUP BY d.name, top_salary"));
        return $q;
    }

    /**
     * @return array
     */
    public function getDepartmentsWith50k()
    {
//        gets just the department names
//        $q = DB::select( DB::raw(
//            "SELECT d.name
//            FROM department d
//            JOIN employees e ON d.dept_id=e.dept_id
//            WHERE d.dept_id IN
//            (select e.dept_id FROM employees e HAVING e.salary > 50000)
//            GROUP BY d.name;"));

//        get ungrouped for further processing
        $q = DB::select( DB::raw("
            SELECT d.name, e.email, e.salary
            FROM department d 
            JOIN employees e ON d.dept_id=e.dept_id 
            WHERE d.dept_id IN
            (select e.dept_id FROM employees e HAVING e.salary > 50000)
        "));

        $department = [];
        foreach($q as $k => $item) {
            $department[$item->name][] = ['email' => $item->email, 'salary' => number_format($item->salary)];
        }
        return $department;
    }

}
