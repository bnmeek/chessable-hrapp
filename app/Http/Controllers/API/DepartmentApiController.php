<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentApiController extends Controller
{

    private $table;

    public function __construct()
    {
        $this->table = DB::table('department');
    }

    public function show($id)
    {
        $department = $this->table->where('dept_id', $id)->first();
        return response()->json($department);
    }

    public function showAll()
    {
        return response()->json($this->table->get());
    }

    public function create(Request $request)
    {
        $department = $this->table->insert(['name' => $request->get('name'), 'table_id' => $request->get('table_id')]);
        return response()->json($department, 201);
    }

    public function update($id, Request $request)
    {
        $department = $this->table->where('table_id', $id)->update($request->all());
        return response()->json($department, 200);
    }

    public function delete(Request $request)
    {
        $id = $request->get('table_id');
        $department = $this->table->where('table_id', $id)->delete();
        return response()->json($department, 200);
    }
}
