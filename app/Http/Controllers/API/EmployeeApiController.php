<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeApiController extends Controller
{

    private $table;

    public function __construct()
    {
        $this->table = DB::table('employees');
    }

    public function show($id)
    {
        $employee = $this->table->where('employee_id', $id)->first();
        return response()->json($employee);
    }

    public function showAll()
    {
        return response()->json($this->table->get());
    }

    public function create(Request $request)
    {
        $employee = $this->table->insert($request->all());
        return response()->json($employee, 201);
    }

    public function update($id, Request $request)
    {
        $employee = $this->table->where('employee_id', $id)->update($request->all());
        return response()->json($employee, 200);
    }

    public function delete($id)
    {
        $employee = $this->table->where('employee_id', $id)->delete();
        return response()->json($employee, 200);
    }
}
